# Symfony Dummy Project

This project was created for **Let's Migrate Symfony Project to Kubernetes!** series:
* [Part 1: Containerizing the Application](https://itnext.io/containerizing-symfony-application-a2a5a3bd5edc)
* [Part 2: Publishing the Application with Helm](https://itnext.io/publishing-symfony-application-with-helm-ecb525b34289)
* [Part 3: Testing the Application](https://medium.com/@babenko.i.a/testing-symfony-application-d02317d4018a)
* [Part 4: Building a Continuous Delivery Pipeline](https://medium.com/@babenko.i.a/building-continuous-delivery-pipeline-2cc05e213935)



## Getting started with the setup.

If you are using Mac OS, you can install Kubernetes CLI (kubectl) and Helm using Homebrew:

```shell
brew install kubernetes-cli kubernetes-helm
helm init --upgrade
```

Now let’s configure the cluster by creating a new namespace for our project:

```shell
kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" create namespace symfony-dummy-project
```

Adding database secret:
```shell
kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" create -n symfony-dummy-project secret generic database-secret --from-literal=database=app --from-literal=password=fP8cz7Q63nV --from-literal=url=mysql://root:fP8cz7Q63nV@mysql:3306/app
```


And starting the Ingress controller as described here:
```shell
kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-0.32.0/deploy/static/provider/cloud/deploy.yaml
```

Install tiller
```shell
kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" --namespace kube-system create sa tiller
kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
```


## server installation done

### local push to DO
```shell
helm upgrade symfony-dummy-project helm --install --set-string phpfpm.env.plain.APP_ENV=stage,nginx.host=symfony.basit.me,imageTag=a28e611b --namespace symfony-dummy-project

```

```shell
helm delete symfony-dummy-project --namespace symfony-dummy-project
```

### helpful commands -

switch to the kubnetes cluster, if there are more then one

```shell
kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" config get-contexts
```


kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" config use-context  do-lon1-k8s-1-16-6-do-2-lon1-1587912614140

kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" get pods --namespace kube-system


