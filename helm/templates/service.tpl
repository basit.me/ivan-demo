{{- define "service.template" -}}
apiVersion: v1
kind: Service
metadata:
  name: {{ .service.name | quote }}
spec:
  ports:
    - targetPort: {{ .service.port }}
      port: {{ .service.port }}
  selector:
    app: {{ .service.name | quote }}
{{- end -}}
