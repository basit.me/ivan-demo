image: docker:latest

services:
  - docker:dind

variables:
  CHART_NAME: symfony-dummy-project
  NAMESPACE: symfony-dummy-project

  # TODO gitlab ci/cd variables
  CLUSTER_NAME_PROD: cluster-name-here
  CLUSTER_NAME_STAGE: cluster-name-here
  CONTAINER_REGISTRY_USER: ''
  CONTAINER_REGISTRY_PASSWORD: ''
  DIGITALOCEAN_ACCESS_TOKEN: ''
  HOST_PROD: ''
  HOST_STAGE: ''

stages:
  - build
  - test
  - registry-update
  - stage-deployment
  - end-to-end-tests
  - production-deployment

.build-template: &buildTemplate
  stage: build
  script:
    # Build the image
    - docker build --build-arg VERSION=$BUILD_VERSION --target $BUILD_TARGET -t $BUILD_IMAGE:$BUILD_TARGET -f $BUILD_DOCKERFILE $BUILD_CONTEXT
  after_script:
    # Save the image as an artifact
    - mkdir -p build/$BUILD_IMAGE
    - docker save $BUILD_IMAGE:$BUILD_TARGET -o build/$BUILD_IMAGE/$BUILD_TARGET.tar
  artifacts:
    name: $CI_JOB_NAME-${CI_COMMIT_SHA:0:8}
    expire_in: 1 day
    paths:
      - build/$BUILD_IMAGE
  dependencies: []

build-php-fpm-test:
  <<: *buildTemplate
  variables:
    BUILD_IMAGE: php-fpm
    BUILD_TARGET: test
    BUILD_DOCKERFILE: docker/php-fpm/Dockerfile
    BUILD_CONTEXT: .
  before_script:
    # Export build version
    - source .env && export BUILD_VERSION=$PHP_VERSION

build-php-fpm-prod:
  <<: *buildTemplate
  variables:
    BUILD_IMAGE: php-fpm
    BUILD_TARGET: prod
    BUILD_DOCKERFILE: docker/php-fpm/Dockerfile
    BUILD_CONTEXT: .
  before_script:
    # Export build version
    - source .env && export BUILD_VERSION=$PHP_VERSION

build-nginx-prod:
  <<: *buildTemplate
  variables:
    BUILD_IMAGE: nginx
    BUILD_TARGET: prod
    BUILD_DOCKERFILE: docker/nginx/Dockerfile
    BUILD_CONTEXT: .
  before_script:
    # Export build version
    - source .env && export BUILD_VERSION=$NGINX_VERSION

build-mysql-prod:
  <<: *buildTemplate
  variables:
    BUILD_IMAGE: mysql
    BUILD_TARGET: prod
    BUILD_DOCKERFILE: docker/mysql/Dockerfile
    BUILD_CONTEXT: docker/mysql
  before_script:
    # Export build version
    - source .env && export BUILD_VERSION=$MYSQL_VERSION

unit-tests:
  stage: test
  before_script:
    # Load php image from the build stage
    - docker load -i build/php-fpm/test.tar
  script:
    - docker run --rm php-fpm:test bin/phpunit --testsuit unit --testdox
  dependencies:
    - build-php-fpm-test

integration-tests:
  stage: test
  before_script:
    # Load php image from the build stage
    - docker load -i build/php-fpm/test.tar
    - docker load -i build/mysql/prod.tar
    # Start mysql
    - source .env
    - docker network create $NAMESPACE-net
    - docker run --rm -d --network $NAMESPACE-net --network-alias mysql -e MYSQL_DATABASE=$MYSQL_DATABASE -e MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD mysql:prod
  script:
    - docker run --rm --network $NAMESPACE-net php-fpm:test sh -c "sh ci/check-connection mysql 3306 && bin/phpunit --testsuit integration --testdox"
  dependencies:
    - build-php-fpm-test
    - build-mysql-prod

.registry-update-template: &registryUpdateTemplate
  stage: registry-update
  before_script:
    # Load image from the build stage
    - docker load -i build/$BUILD_IMAGE/$BUILD_TARGET.tar
  script:
    # Tag the image
    - docker tag $BUILD_IMAGE:$BUILD_TARGET basitme/$BUILD_IMAGE:${CI_COMMIT_SHA:0:8}
    # Push the image
    - echo $CONTAINER_REGISTRY_PASSWORD | docker login -u $CONTAINER_REGISTRY_USER --password-stdin
    - docker push basitme/$BUILD_IMAGE:${CI_COMMIT_SHA:0:8}
  only:
    - master

registry-update-php-fpm:
  <<: *registryUpdateTemplate
  variables:
    BUILD_IMAGE: php-fpm
    BUILD_TARGET: prod
  dependencies:
    - build-php-fpm-prod

registry-update-nginx:
  <<: *registryUpdateTemplate
  variables:
    BUILD_IMAGE: nginx
    BUILD_TARGET: prod
  dependencies:
    - build-nginx-prod

registry-update-mysql:
  <<: *registryUpdateTemplate
  variables:
    BUILD_IMAGE: mysql
    BUILD_TARGET: prod
  dependencies:
    - build-mysql-prod

.deploy-template: &deployTemplate
  image: basitme/doctl-kubectl
  variables:
    DIGITALOCEAN_ACCESS_TOKEN: $DIGITALOCEAN_ACCESS_TOKEN
  before_script:
    # Fetch cluster credentials
    - sh ci/fetch-cluster-credentials $CLUSTER_NAME
  script:
    - kubectl version
    - helm version
    - helm upgrade $CHART_NAME helm --install --set-string phpfpm.env.plain.APP_ENV=$ENVIRONMENT,nginx.host=$HOST,imageTag=${CI_COMMIT_SHA:0:8} --namespace $NAMESPACE
  dependencies: []
  only:
    - master

deploy-staging:
  <<: *deployTemplate
  variables:
    ENVIRONMENT: stage
    CLUSTER_NAME: $CLUSTER_NAME_STAGE
    HOST: $HOST_STAGE
  stage: stage-deployment
  after_script:
    # Wait for the rollout to complete
    - sh ci/wait-for-rollout $CHART_NAME

deploy-production:
  <<: *deployTemplate
  variables:
    ENVIRONMENT: prod
    CLUSTER_NAME: $CLUSTER_NAME_PROD
    HOST: $HOST_PROD
  stage: production-deployment
  when: manual

end-to-end-test:
  stage: end-to-end-tests
  image:
    name: postman/newman:alpine
    entrypoint: [""]
  script:
    - newman run -e tests/EndToEnd/env.stage.json tests/EndToEnd/collection.json
  dependencies: []
  only:
    - master

.rollback-template: &rollbackTemplate
  image: basitme/doctl-kubectl
  before_script:
    # Fetch cluster credentials
    - sh ci/fetch-cluster-credentials $CLUSTER_NAME
  script:
    - helm rollback $CHART_NAME 0 --namespace $NAMESPACE
  dependencies: []
  when: manual
  only:
    - master

rollback-staging:
  <<: *rollbackTemplate
  variables:
    CLUSTER_NAME: $CLUSTER_NAME_STAGE
  stage: stage-deployment

rollback-production:
  <<: *rollbackTemplate
  variables:
    CLUSTER_NAME: $CLUSTER_NAME_PROD
  stage: production-deployment
