.PHONY: _list

_list:
	@echo "Type make then a space then hit tab to see available commands"

env ?= dev

build:
	docker-compose --project-name=demo_app build --pull

up:
	docker-compose --project-name=demo_app up

upd:
	docker-compose --project-name=demo_app up -d

stop:
	docker-compose --project-name=demo_app stop

start:
	docker-compose --project-name=demo_app start

rm:
	docker-compose --project-name=demo_app rm

shell:
	docker exec -t -i demo_app bash

clear_cache:
	docker exec demo_app rm -rf /project/var/cache/*
	docker exec demo_app chmod -R 777 /project/var/cache
	docker exec demo_app chmod -R 777 /project/var/logs
	docker exec demo_app chmod -R 777 /project/var/sessions

install-assets:
	docker exec demo_app ./project/install-asset.sh

test-api:
	docker exec demo_app /usr/local/bin/php /project/bin/console test:connect --env=prod
