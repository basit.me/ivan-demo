# change helm/values.yaml if needed to
# change tests/EndToEnd/env.stage.json for base url value



# create a namespace
kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" create namespace symfony-dummy-project

# add env variables for database
kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" create -n symfony-dummy-project secret generic database-secret --from-literal=database=app --from-literal=password=fP8cz7Q63nV --from-literal=url=mysql://root:fP8cz7Q63nV@mysql:3306/app

# install tiller on cluster
kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" --namespace kube-system create sa tiller
kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller

# create traefik now
### check traefik project
### also add ip to domain


#### server installation done

# helpful commands -
# switch to the kubnetes cluster, if there are more then one
kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" config get-contexts


kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" config use-context  do-lon1-k8s-1-16-6-do-2-lon1-1587912614140

kubectl --kubeconfig="k8s-digitalocean-kubeconfig.yaml" get pods --namespace kube-system


## local push to DO
helm upgrade symfony-dummy-project helm --install --set-string phpfpm.env.plain.APP_ENV=stage,nginx.host=symfony.basit.me,imageTag=a28e611b --namespace symfony-dummy-project


helm delete symfony-dummy-project --namespace symfony-dummy-project
